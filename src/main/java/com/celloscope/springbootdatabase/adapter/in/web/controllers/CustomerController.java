package com.celloscope.springbootdatabase.adapter.in.web.controllers;

import com.celloscope.springbootdatabase.application.port.in.CustomerUseCase;
import com.celloscope.springbootdatabase.application.service.CustomerService;
import com.celloscope.springbootdatabase.domain.Customer;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CustomerController {

    private final CustomerUseCase customerUseCase;
    
    @Autowired
    CustomerService customerservice;
    

    public CustomerController(CustomerUseCase customerUseCase) {
        this.customerUseCase = customerUseCase;
    }

    @GetMapping("/customers/{id}")
    public ResponseEntity<Optional<Customer>> findCustomer(@PathVariable Long id){
       // Customer customer = customerUseCase.getCustomer(id);
    	
    	Optional<Customer> customer =customerservice.findCustomer(id);
        if (customer != null) return ResponseEntity.ok(customer);
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/customers")
    public ResponseEntity<String> saveCustomer(@RequestBody Customer customer ){
        
    	
    	System.out.println(" name "+customer.getName());
		/*
		 * String customerId = customerUseCase.saveCustomer(customer); if (customerId !=
		 * null && !customerId.isEmpty()) return ResponseEntity.ok(customerId);
		 */
    	List<Customer> cs= customerservice.addCustomer(customer);
    	
        return ResponseEntity.ok("Save and new Customer Id is "+cs.get(0).getId());
    }
}
