package com.celloscope.springbootdatabase.application.service;

import com.celloscope.springbootdatabase.application.port.in.CustomerUseCase;
import com.celloscope.springbootdatabase.application.port.out.CustomerPersistencePort;
import com.celloscope.springbootdatabase.domain.Customer;
import com.celloscope.springbootdatabase.repository.CustomerRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService implements CustomerUseCase {

    private final CustomerPersistencePort customerPersistencePort;
    @Autowired
    CustomerRepository customerrep;

    public CustomerService(CustomerPersistencePort customerPersistencePort) {
        this.customerPersistencePort = customerPersistencePort;
    }

    @Override
    public Customer getCustomer(String customerId) {
        return customerPersistencePort.get(customerId);
    }

    @Override
    public String saveCustomer(Customer customer) {
        return customerPersistencePort.save(customer);
    }
    
    
    
    public Optional<Customer> findCustomer(Long customerid) {
    	
    	Optional<Customer> customer=customerrep.findById(customerid);
    	return customer;
    	
    }
    
    
    public List<Customer> addCustomer(Customer customer) {
    	
    	Customer cs=new Customer();
    	cs.setName(customer.getName());
    	cs.setPhoneNo(customer.getPhoneNo());
    	customerrep.save(cs);
    	List<Customer> custo=customerrep.findTopByOrderByIdDesc();
    	
    	return custo;
    	
    }
}
